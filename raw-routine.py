from epics import PV
from time import sleep

# dvm PVs
dvm_run = PV("DVM:RUN")
dvm_measurement = PV("DVM:MEASUREMENT")
dvm_send = PV("DVM:SEND")
dvm_nrdgs = PV("DVM:NRDGS")

# trigger PVs
trigger_stepsize_wr = PV("PMAC:TRIGGER:StepSize_Wr")
trigger_arraylength_wr = PV("PMAC:TRIGGER:ArrayLength_Wr")
trigger_run = PV("PMAC:TRIGGER:cmdRun_Wr")

# pmac
pmac_cmd_send = PV("cmd")

# motor PVs
motor_pos = PV("PMAC:MOTOR:M1.RBV")
motor_dest = PV("PMAC:MOTOR:M1.VAL")
motor_speed = PV("PMAC:MOTOR:M1.S")

# create function to wait motor to finish
def run_movement(destination, speed):
    initial = motor_pos.get()
    motor_speed.put(speed)
    movement_time = (abs(destination-initial)/65535)/speed
    motor_dest.put(destination)
    sleep(movement_time + 2) # 1 second extra
    print(f"Current Position =  {motor_pos.get()}" )

def run_measurement(step_size, destination, speed):
    trigger_stepsize_wr.put(step_size)
    array_length = destination/step_size
    trigger_arraylength_wr.put(array_length)
    print(array_length)
    sleep(1)
    dvm_nrdgs.put(array_length-1)
    dvm_run.put(1)
    sleep(1)
    run_movement(destination,speed)
    sleep(1)
    dvm_run.put(0)

def reset_states():
    run_movement(destination=0, speed=1) # go to 
    pmac_cmd_send.put("disable plc trigger_dvm")
    sleep(0.5)
    pmac_cmd_send.put("enable plc trigger_dvm")
    sleep(0.5)
    trigger_run.put(0) # stop triggering process - this resets counter and PrevPos
    sleep(0.5)
    trigger_run.put(1) # start triggering process
    sleep(0.5)
    dvm_send.put("RESET")
    
reset_states()
sleep(2)
run_measurement(step_size=1000,destination=10*65535,speed=1)
sleep(2)


def read_pmac_array(initial_record_number, records_count):
    values_array =[]
    for i in range(initial_record_number,initial_record_number+records_count):
        pv_name = PV(f"PMAC:ARRAY:P{i}")
        pv_value = pv_name.get()
        values_array.append(pv_value)
    return values_array

pmac_arr = read_pmac_array(8192,100)
print(pmac_arr)
