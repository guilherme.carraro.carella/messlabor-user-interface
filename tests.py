from ophyd import EpicsMotor
from bluesky import RunEngine
from bluesky.callbacks import LiveTable, LivePlot
from bluesky.plans import count
from databroker.v2 import temp

import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np

m1 = EpicsMotor('motor', name='m1')

db = temp()

RE = RunEngine()

RE.subscribe(db.v1.insert)

RE(count([m1.position],num=100,delay=0.1))

run = db[-1]

data = run.primary.read().m1
df = data.to_dataframe()

print(df)
